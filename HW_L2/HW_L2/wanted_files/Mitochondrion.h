#pragma once
#include "Protein.h"
#include "AminoAcid.h"

class Mitochondrion 
{
	unsigned int _glocuse_level;
	bool _receptor_glocuse_has;

	public:
		void init();
		void insert_glucose_receptor(const Protein& protein);
		void set_glucose(const unsigned int glocuse_units);
		bool produceATP() const;
};
