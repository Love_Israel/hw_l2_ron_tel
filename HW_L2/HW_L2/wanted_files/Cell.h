#pragma once

#include <iostream>
#include "Ribosome.h"
#include "Mitochondrion.h"
#include "Nucleus.h"
#include "Protein.h"

using std::string;

class Cell 
{
	Gene _glucose_receptor_gene;
	Mitochondrion _mitochondrion;
	Ribosome _ribosome;
	Nucleus _nucleus;

	unsigned int _atp_units;

	public:
		void init(const string dna_sequence, const Gene glucose_receptor_gene);
		bool get_ATP();
		Nucleus get_Nucleus();
		Gene get_gene();

};
