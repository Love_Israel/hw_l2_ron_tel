#include <iostream>
#include <string>

#include "AminoAcid.h"
#include "Protein.h"
#include "Ribosome.h"

using std::string;

Protein* Ribosome :: create_protein (string& RNA_transcript) const 
{
	Protein* Protein = new LinkedList; //make new Protein (linked list) object
	Protein->init();
	string nucleide = "";

	for (int i = 0 ; i < RNA_transcript.length() ; i+=3) 
	{
		nucleide = RNA_transcript.substr(i, 3); //get the codon
		if (get_amino_acid(nucleide) == UNKNOWN) //check there is amino acid for the codon
		{
			std::cerr << "cant create protein" << "\n";
			Protein->clear();
			return NULL;
		}
		Protein->add(get_amino_acid(nucleide)); //add the amino acid to the list
	}
	return Protein;
}