#include "Protein.h"
#include "AminoAcid.h"
#include "Mitochondrion.h"

#include <iostream>

using std::string;

void Mitochondrion :: init() 
{
	this->_glocuse_level = 0;
	this->_receptor_glocuse_has = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	//amino acid order to make glucose
	AminoAcid order[7] =
	{ ALANINE , LEUCINE , GLYCINE , HISTIDINE , LEUCINE , PHENYLALANINE
	, AMINO_CHAIN_END };

	AminoAcidNode* currNode = protein.get_first();

	for (int i = 0 ; i < 7 ; i++) 
	{
		if (currNode->get_data() != order[i]) //if not in right order
			return;
		if (i == 6 && currNode->get_next()) //if longer then it should be
			return;
		currNode = currNode->get_next();
	}

	this->_receptor_glocuse_has = true;
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units) 
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion :: produceATP() const 
{
	return (this->_receptor_glocuse_has && this->_glocuse_level >= 50);
}
